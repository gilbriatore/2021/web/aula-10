
const main = document.querySelector('main');

// Requisição AJAX (Assíncrona).
function carregarHTML(url){
  fetch(url)
  .then(res => res.text())
  .then(html => {
     main.innerHTML = html;   
   });
}

function carregarHome(){
  fetch('html/home.html')
  .then(res => res.text())
  .then(html => {
    main.innerHTML = html;   
    fetch('http://localhost:8000/pratododia')
        .then(res => res.json())
        .then(pratoDoDia => {
          const imgFoto = document.querySelector("section > img");
          imgFoto.src = "../img/" + pratoDoDia.foto;
          const h1Nome = document.querySelector("section > div > h1");
          h1Nome.innerHTML = pratoDoDia.nome;
          const h2Preco = document.querySelector("section > div > h2.home-prato-preco");
          h2Preco.innerHTML = pratoDoDia.preco;
          const pDescricao = document.querySelector("section > div > p");
          pDescricao.innerHTML = pratoDoDia.descricao;
        });
   });
}

carregarHome();

/* 
<div class="card-prato">
  <span class="card-titulo">Acompanhamentos</span>
  <img src="./img/cafe1.jpg">
  <div class="card-conteudo">
    <span class="card-preco">R$&nbsp;27,56</span>
    <span class="card-texto">Manteiga, nata, mel, requeijão salgado, queijo branco.</span>
    <button class="card-texto">Adicionar ao carrinho</button>
  </div>
</div> 
*/

// "id": 2,
// "nome": "Bruschetta de salmão e abacate",
// "descricao": "Pão preto, salmão defumado, queijo Philadelphia, abacate, tomate, ovos.",
// "preco": 52.49,
// "peso": 180,
// "foto": "cafe2.jpg",
// "desconto": 10,
// "pontosDasAvaliacoes": 4,
// "totalDeAvaliacoes": 320,
// "disponibilidade": 7,
// "totalDeCompras": 0,
// "ehVegetariano": false,
// "categoriaId": 1

function criarCard(produto){

  const divPrato = document.createElement('div');
  divPrato.classList.add('card-prato');
  const spanTitulo = document.createElement('span');
  spanTitulo.classList.add('card-titulo');
  spanTitulo.innerHTML = produto.nome;

  const imgFoto = document.createElement('img');
  imgFoto.src = "../img/" + produto.foto;

  const divConteudo = document.createElement('div');
  divConteudo.classList.add('card-conteudo');
  const spanPreco = document.createElement('span');
  spanPreco.classList.add('card-preco');
  spanPreco.innerHTML = produto.preco;

  const spanTexto = document.createElement('span');
  spanTexto.classList.add('card-texto');
  spanTexto.innerHTML = produto.descricao;

  const btnAdicionar = document.createElement('button');
  btnAdicionar.classList.add('card-texto');
  btnAdicionar.innerHTML= "Adicionar ao carrinho";
  btnAdicionar.onclick = function(e){
    alert("Programa a função do botão!");
  }

  divConteudo.appendChild(spanPreco);
  divConteudo.appendChild(spanTexto);
  divConteudo.appendChild(btnAdicionar);

  divPrato.appendChild(spanTitulo);
  divPrato.appendChild(imgFoto);
  divPrato.appendChild(divConteudo);
  return divPrato;
}

function carregarCardapio(){
  fetch('html/cardapio.html')
  .then(res => res.text())
  .then(html => {
    main.innerHTML = html;   
    const section = document.querySelector('section');
    fetch('http://localhost:8000/produtos')
        .then(res => res.json())
        .then(produtos => {
          produtos.forEach(produto => {
              const card = criarCard(produto);
              section.appendChild(card);
          });
        });
   });
}

// function carregarCarrinho(){
//   fetch('html/carrinho.html')
//   .then(res => res.text())
//   .then(html => {
//      main.innerHTML = html;   
//    });
// }



// <div id="status" style="display: block;" class="tabcontent">
//   <h3>Editar status</h3>
//   <div>
//     <form name="status" action="/#" method="post">
//       <input id="id" name="id" type="hidden">
//       <div>
//         <label for="nome">Nome</label>
//         <input id="nome" name="nome" type="text">
//       </div>
//       <label></label>
//       <input id="btnSalvar" type="button" value="Salvar">
//       <input id="btnCancelar" type="button" value="Cancelar">
//     </form>
//   </div>
// </div>
function editarStatus(id){
  fetch('html/status/status_editar.html')
    .then(res => res.text())
    .then(html => {
      main.innerHTML = html;  
      fetch('http://localhost:8000/status/' + id)
      .then(res => res.json())
      .then(status => {
        const inputId = document.forms.status.id;
        inputId.value = status.id;
        const inputNome = document.forms.status.nome;
        inputNome.value = status.nome;
        const btnSalvar = document.getElementById('btnSalvar');
        btnSalvar.onclick = function(e){
          e.preventDefault();
          
          const inputId = document.forms.status.id;
          const inputNome = document.forms.status.nome;

          const header = {
            method: 'PUT',
            headers: {
              'Accept' : 'application/json',
              'Content-type': 'application/json'
            },
            body: JSON.stringify({
              "id": inputId.value,
              "nome": inputNome.value
            })
          }

          fetch("http://localhost:8000/status/" + inputId.value, header)
            .then(() => {
              carregarListaDeStatus();
            });
        }
        const btnCancelar = document.getElementById('btnCancelar');
        btnCancelar.onclick = function(e){
          e.preventDefault();
          carregarListaDeStatus();
        }
        
      });
  });
}

function incluirStatus(){
  fetch('html/status/status_incluir.html')
    .then(res => res.text())
    .then(html => {
      main.innerHTML = html;  
      const btnSalvar = document.getElementById('btnSalvar');
      btnSalvar.onclick = function(e){
        e.preventDefault();

        const inputNome = document.forms.status.nome;

        const header = {
          method: 'POST',
          headers: {
            'Accept' : 'application/json',
            'Content-type': 'application/json'
          },
          body: JSON.stringify({
            "nome": inputNome.value
          })
        }

        fetch("http://localhost:8000/status", header)
          .then(() => {
            carregarListaDeStatus();
          });
      }
      const btnCancelar = document.getElementById('btnCancelar');
      btnCancelar.onclick = function(e){
        e.preventDefault();
        carregarListaDeStatus();
      }
  });
}

function carregarListaDeStatus(){
  fetch('html/status/status_listar.html')
    .then(res => res.text())
    .then(html => {
      main.innerHTML = html;   
      const tbody = document.querySelector("#status > table > tbody");
      fetch('http://localhost:8000/status')
          .then(res => res.json())
          .then(listaDeStatus=> {

            const btnIncluir = document.querySelector("#status > div > a");  
            btnIncluir.onclick = function(e) {
              e.preventDefault();
              incluirStatus();
            }  

            listaDeStatus.forEach(status => {
                const linhaDeStatus = criarLinhaDeStatus(status);
                tbody.appendChild(linhaDeStatus);
            });
          });
    });
}

function excluirStatus(id){
  fetch('html/status/status_excluir.html')
  .then(res => res.text())
  .then(html => {
    main.innerHTML = html;  
    fetch('http://localhost:8000/status/' + id)
    .then(res => res.json())
    .then(status => {
      const inputId = document.forms.status.id;
      inputId.value = status.id;
      const inputNome = document.forms.status.nome;
      inputNome.value = status.nome;
      const btnConfirmar = document.getElementById('btnConfimar');
      btnConfirmar.onclick = function(e){
        e.preventDefault();
        
        const header = {
          method: 'DELETE',
          headers: {
            'Accept' : 'application/json',
            'Content-type': 'application/json'
          }
        }
      
        fetch("http://localhost:8000/status/" + id, header)
          .then(() => {
            carregarListaDeStatus();
          });
      }
      const btnCancelar = document.getElementById('btnCancelar');
      btnCancelar.onclick = function(e){
        e.preventDefault();
        carregarListaDeStatus();
      }
      
    });
  });
}

function criarLinhaDeStatus(status){
  //   <tr>
  //     <td>1</td>
  //     <td>Em processamento</td>
  //     <td>
  //       <a href="/status_editar.html" style="display: inline-block; width: 40px;">Editar</a>
  //       <a href="/status_excluir.html">Excluir</a>
  //     </td>
  //   </tr>
  const trLinha = document.createElement('tr');
  const tdId = document.createElement('td');
  tdId.innerHTML = status.id;
  const tdNome = document.createElement('td');
  tdNome.innerHTML = status.nome;
  const tdAcoes = document.createElement('td');
  const aEditar = document.createElement('a');
  aEditar.href = '';
  aEditar.style.display = 'inline-block';
  aEditar.style.width = '40px';
  aEditar.onclick = function(e) {
    e.preventDefault();
    editarStatus(status.id);
  }
  aEditar.innerHTML = 'Editar';

  const aExcluir = document.createElement('a');  
  aExcluir.href = '';
  aExcluir.onclick = function(e) {
    e.preventDefault();
    excluirStatus(status.id);
  }
  aExcluir.innerHTML = 'Excluir';

  tdAcoes.appendChild(aEditar);
  tdAcoes.appendChild(aExcluir);

  trLinha.appendChild(tdId);
  trLinha.appendChild(tdNome);
  trLinha.appendChild(tdAcoes);

  return trLinha;
}
 
function carregarCadastros(){
  fetch('html/cadastros.html')
  .then(res => res.text())
  .then(html => {
     main.innerHTML = html;  
     const btnListarProdutos = document.querySelector('#btnListarProdutos');
     btnListarProdutos.onclick = function(e){
      e.preventDefault();
      alert('Clique em listar produtos!');
     }
     const btnListarClientes = document.querySelector('#btnListarClientes');
     btnListarClientes.onclick = function(e){
      e.preventDefault();
      alert('Clique em listar clientes!');
     }
     const btnListarPedidos = document.querySelector('#btnListarPedidos');
     btnListarPedidos.onclick = function(e){
      e.preventDefault();
      alert('Clique em listar pedidos!');
     }
     const btnListarCategorias = document.querySelector('#btnListarCategorias');
     btnListarCategorias.onclick = function(e){
      e.preventDefault();
      alert('Clique em listar categorias!');
     }
     const btnListarStatus = document.querySelector('#btnListarStatus');
     btnListarStatus.onclick = function(e){
      e.preventDefault();
      carregarListaDeStatus();
     }
     const btnPratoDoDia = document.querySelector('#btnPratoDoDia');
     btnPratoDoDia.onclick = function(e){
      e.preventDefault();
      alert('Clique em prato do dia!');
     }
     const btnLogado = document.querySelector('#btnLogado');
     btnLogado.onclick = function(e){
      e.preventDefault();
      alert('Clique em cliente logado!');
     }



   });
}

// function carregarSobre(){
//   fetch('html/sobre.html')
//   .then(res => res.text())
//   .then(html => {
//      main.innerHTML = html;   
//    });
// }


// Eventos (onclick)
const mnHome = document.querySelector('#menuHome');
mnHome.onclick = function(e){
  e.preventDefault();
  carregarHome();
}
const mnCardapio = document.querySelector('#menuCardapio');
mnCardapio.onclick = function(e){
  e.preventDefault();
  carregarCardapio();
}
const mnCarrinho = document.querySelector('#menuCarrinho');
mnCarrinho.onclick = function(e){
  e.preventDefault();
  carregarHTML('html/carrinho.html');
}
const mnCadastros = document.querySelector('#menuCadastros');
mnCadastros.onclick = function(e){
  e.preventDefault();
  carregarCadastros();
}
const mnSobre = document.querySelector('#menuSobre');
mnSobre.onclick = function(e){
  e.preventDefault();
  carregarHTML('html/sobre.html');
}